# text-2-ssyk

Classifies a text into SSYK level 1-4 based on one or several multiclass classifiers.\
Training use datasets from [historical jobs](https://jobtechdev.se/docs/apis/historical/).

## Models

### 1. Baseline model
- training_model_baseline.py
- Bag-of-words (15k features) + random forest classifier (30 trees, max depth 100)

#### Performance 
- 5-fold cross-validation
- Uniformly drawn samples

| Sample size [nr ads] | Exact score SSYK level 4 [%] | In top 3 [%] | In top 5 [%] |
|------------:|-------:|-------:|-------:|
| 50k |   58.2 ± 0.8  |  71.8 ± 0.8  |  75.7 ± 0.7 |
| 100k |  62.5 ± 0.3 |  75.5 ± 0.2 | 79.0 ± 0.2 |
| 500k | 72.0 ± 0.1 | 82.5 ± 0.1 | 85.2 ± 0.1 | 
| 1M | 76.2 ± 0.1 | 85.6 ± 0.1 | 87.8 ± 0.1 | 

### 2. Word vectors + backpropagation
- training_model_vectors.py
- Word vectors derived during training (not pre-set)

#### Performance

300k train, 100k test: 75.3%

### 3. Enriched model
- training_model_enrich.py
- Bag of words + random forest classifier.
- Input set to all enriched candidates (occupations, competencies and traits) extracted from the free text and headline (using the JobAd Enrichment API).
- Feature size as all available enriched candidates (~36k)

#### Performance

16k train, 4k test: 61.6%  
80k train, 20k test: 67.8%  
160k train, 40k test: 73.1%  
300k train, 100k test: 73.6%

## Joblinks Docker example

    docker build -t text2ssyk .
    echo '[{ "description":"baka pizza" },{ "description":"städa kontor"}]' | docker run -i text2ssyk