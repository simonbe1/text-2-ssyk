import json
import random
import requests




def load_data_enriched_features():
    
    return []

def load_data():

    nrSamples = 500000
    useSSYKLevel = 4
    pathData = 'data/'
    pathModels = 'models/'

    useStratifiedSampling = False # not implemented

    with open(pathData + 'occId_2_ssyk2012.json') as f:
        occId2ssyk = json.load(f)

    with open(pathData + 'occId_2_name.json') as f:
        yrkeId2Name = json.load(f)

    filenames = [pathData + '2019.json', pathData + '2018.json']
    usePreSampled = True

    if usePreSampled:
        filenames = [pathData + 'sample_'+str(nrSamples)+'.json']#[pathData + 'sample.json']
    
    print('loading',filenames)

    ads = []

    for filename in filenames:
        with open(filename) as fp:
            ads.extend(json.load(fp))

    print('nr ads:',len(ads))

    if usePreSampled == False: # assumes always sampling

        if useStratifiedSampling: # stratified based on occupations
            print('ERROR: not implemented')
        else: # uniform
            sampleIndices = random.sample(range(0,len(ads)), nrSamples)
            ads = [ads[i] for i in sampleIndices]

        print('saving sampled data')

        with open(pathData + 'sample_'+str(nrSamples)+'.json','w') as f:
            json.dump(ads,f)

    print('nr ads:',len(ads))

    X_text = []
    X_headline = []
    y_occId = []
    y_ssyk = []
    check_not_existing = []

    #  removing items which have an older (not used anymore) occupation id
    for ad in ads:
        
        #X_headline.append(ad['headline'])
        occId = int(ad['occupation']['legacy_ams_taxonomy_id'])

        if str(occId) in occId2ssyk:
            ssyk = int(occId2ssyk[str(occId)])
            X_text.append(ad['headline'] + ' ' + ad['description']['text'])

            if useSSYKLevel == 1:
                ssyk = int(ssyk/1000)
            elif useSSYKLevel == 2:
                ssyk = int(ssyk/100)
            elif useSSYKLevel == 3:
                ssyk = int(ssyk/10)

            y_ssyk.append(ssyk)
        else:
            check_not_existing.append(str(occId))
            ssyk = -1

        #y_occId.append(occId)

    return [X_text, y_ssyk]


def load_enriched_data(max_items = -1):

    pathData = 'data/'
    filenames = [pathData + 'enriched_full_2019_no_sentences.json',pathData + 'enriched_full_2018_no_sentences.json']

    with open(pathData + 'occId_2_ssyk2012.json') as f:
        occId2ssyk = json.load(f)

    with open(pathData + 'occId_2_name.json') as f:
        yrkeId2Name = json.load(f)

    ads = []

    for filename in filenames:
        print('loading',filename)
        with open(filename) as fp:
            ads.extend(json.load(fp))
    
    print(len(ads))

    all_concepts = {}
    data = []

    for i,ad in enumerate(ads):

        ad_concepts = {}

        competencies = ad['enriched_candidates']['competencies']
        occupations = ad['enriched_candidates']['occupations']
        traits = ad['enriched_candidates']['traits']

        candidates = []
        candidates.extend(competencies)
        candidates.extend(occupations)
        candidates.extend(traits)

        for c in candidates:
            label = c['concept_label']
            if label not in all_concepts:
                all_concepts[label] = 0
            if label not in ad_concepts:
                ad_concepts[label] = 0

            all_concepts[label] += 1
            ad_concepts[label] += 1

        if 'ssyk' in ad:
            d = {'ssyk': ad['ssyk'], 'legacy_ams_taxonomy_id': ad['legacy_ams_taxonomy_id'], 'id': ad['doc_id'], 'concepts': sorted(list(ad_concepts.items()), key=lambda x: x[1], reverse=True) }
            data.append(d)

        if max_items > -1 and i == max_items:
            return [sorted(list(all_concepts.items()), key=lambda x: x[1], reverse=True), data]
            
        
    return [sorted(list(all_concepts.items()), key=lambda x: x[1], reverse=True), data]



    

def enrich_batch(batch, api_key):

    adr = "https://jobad-enrichments-api.jobtechdev.se/enrichtextdocuments"
    
    headers = {
    "api-key": api_key,
    "accept": "application/json",
    "Content-Type": "application/json"
    }

    input_data = { 'documents_input': [ ], 'include_terms_info': False, 'include_sentences': False, 'sort_by_prediction_score': 'NOT_SORTED'}

    for d in batch:
        doc = { 'doc_id': d['id'], 'doc_headline':d['headline'], 'doc_text': d['description']['text'] }
        input_data['documents_input'].append(doc)

    test2 = {
        'documents_input': [ 
            {
                'doc_id': '123ABC',
                "doc_headline": "Systemutvecklare",
                "doc_text": "Vi letar efter en vass javautvecklare. Du ska vara noggrann, snabb och trevlig. Du måste kunna Java, Python, AngularJS och Cobol. Du kommer att jobba på vårt kontor i Ystad. Vårt huvudkontor ligger i Kiruna"
            },
        ],
        "include_terms_info": 'false',
        "include_sentences": False,
        "sort_by_prediction_score": "NOT_SORTED"
    }

    #test3 = json.dumps(test2)
    input_req = json.dumps(input_data)
    
    r = requests.post(adr, headers=headers, data = input_req)#{'key':'value'})

    return r.json()

def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]

def generate_enriched_features(data, api_key):

#    headers2 = { 'accept': 'application/json', "api-key": api_key }
#    r0 = requests.get('https://jobad-enrichments-api.jobtechdev.se/synonymdictionary?type=COMPETENCE&spelling=CORRECTLY_SPELLED', headers=headers2)

    all_res = []

    # enriches 100 items at a time
    for i,x in enumerate(batch(range(0, len(data)), 100)):
        data_batch = [data[i] for i in x]
        res = enrich_batch(data_batch, api_key)
        all_res.extend(res)
        if i%10 == 0:
            print(len(all_res))

    # make into proper training data

    return all_res

def load_and_make_enriched_train_set(api_key):
    
    pathData = 'data/'

    with open(pathData + 'occId_2_ssyk2012.json') as f:
        occId2ssyk = json.load(f)

    with open(pathData + 'occId_2_name.json') as f:
        yrkeId2Name = json.load(f)

    filenames = [pathData + '2018.json']#'sample_5000.json']
#    filenames = [pathData + '2019.json', pathData + '2018.json']

    ads = []
    id2ad = {}
    for filename in filenames:
        with open(filename) as fp:
            ads.extend(json.load(fp))
            
    for ad in ads:
        id2ad[ad['id']] = ad

    print('nr ads:',len(ads))
    
    res = generate_enriched_features(ads, api_key)

    print('nr enriched:', len(res))

    # attach metadata
    filtered_full = []

    for i,r in enumerate(res):
        doc_id = r['doc_id']
        ad = id2ad[int(doc_id)]
        legacy_ams_taxonomy_id = ad['occupation']['legacy_ams_taxonomy_id']
        if legacy_ams_taxonomy_id in occId2ssyk:
            ssyk = occId2ssyk[legacy_ams_taxonomy_id]
            r['legacy_ams_taxonomy_id'] = legacy_ams_taxonomy_id
            r['ssyk'] = ssyk
            filtered_full.append(r)

    # save different versions
    with open('c:/temp/enriched_full_2018_no_sentences.json','w') as f:
        json.dump(filtered_full,f)

    return res
