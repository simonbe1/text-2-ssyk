import json

import os
import sys
import joblinks_classify

input_json = ''
#input_json = os.getenv('input_json')

data = []

for line in sys.stdin:
    try:
        data.append(json.loads(line))
    except:
        print('Could not load json from string:' + str(line), file=sys.stderr)
        exit(1)

if len(data) > 0:
    data = joblinks_classify.load_and_classify(data)
else:
    print('No data input', file=sys.stderr)
    exit(1)

#os.environ['output_json'] = output_json

for d in data:
    print(json.dumps(d))