import json
import numpy as np
import pickle
#import joblib
from sklearn.externals import joblib

import os
import sys
import help_functions

isDebug = False
pathModels = ''

filenameModel = pathModels + 'model_random_forest_300000_15000.gzip'
filenameVectorizer = pathModels + 'vector_300000_15000.pickle'
filenameTfid = pathModels + 'tfid_300000_15000.pickle'

def classifyTexts(inputTexts, vectorizer, tfidconverter, classifier):
    X = [help_functions.preprocessText(s) for s in inputTexts]
    X = vectorizer.transform(X)
    X = tfidconverter.transform(X)

    #res = classifier.predict(X)
    rTop = classifier.predict_proba(X)

    n = 5
    rTop = np.argsort(rTop)[:,:-n-1:-1]
    resTop5 = []
    for r in rTop:
        rs = [classifier.classes_[i] for i in r]
        resTop5.append(rs)

    return resTop5

def load_and_classify(data):

    if isDebug: print('loading:',filenameVectorizer)
    vectorizer = pickle.load(open(filenameVectorizer, "rb"))

    if isDebug: print('loading:',filenameTfid)
    tfidconverter = pickle.load(open(filenameTfid,"rb"))

    if isDebug: print('loading:',filenameModel)
    classifier = joblib.load(filenameModel)

    # load help mappings
    #occIdName = json.load(open('../data/occId_2_name.json','rb'))
    #ssykName = {}

    if isDebug: print('data:',data)

    if len(data) > 0:
        inputTexts = [d['originalJobPosting']['description'] for d in data]

        resTop5 = classifyTexts(inputTexts, vectorizer, tfidconverter, classifier)

        # need to go from numpy to python int to make json serializable
        for i,r in enumerate(resTop5):
            data[i]['ssyk_lvl4'] = int(r[0])
            #data[i]['ssyk_lvl4_statistical_top5'] = [int(ssyk) for ssyk in r]

    return data