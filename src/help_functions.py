import json
import numpy as np
import re
#from nltk.stem import WordNetLemmatizer

def preprocessText(s):

    #stemmer = WordNetLemmatizer()

    # the special characters
    sNew = re.sub(r'\W', ' ', str(s))
    
    # single characters
    sNew = re.sub(r'\s+[a-zA-Z]\s+', ' ', sNew)
    
    # single characters from the start
    sNew = re.sub(r'\^[a-zA-Z]\s+', ' ', sNew) 
    
    # multiple spaces with single space
    sNew = re.sub(r'\s+', ' ', sNew, flags=re.I)
    
    # prefixed 'b'
    sNew = re.sub(r'^b\s+', '', sNew)
    
    # Lowercase
    sNew = sNew.lower()
    
    # Lemmatization
    sNew = sNew.split()

    #sNew = [stemmer.lemmatize(word) for word in sNew]
    sNew = ' '.join(sNew)
    
    return sNew


def occId_converts(occ_id):

    ssyk = occ_id

    return { 'id': occ_id, 'id_name': '', 'ssyk': ssyk,  'ssyk_name': '' }