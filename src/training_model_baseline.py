import json
import numpy as np
import re
import nltk
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.corpus import stopwords
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
import pickle
from sklearn.externals import joblib

from sklearn.model_selection import StratifiedKFold

import help_functions
import random


# Baseline model

# --------------------------------------------------
# Results 5-fold cross-validation (80/20 train/test)
# --------------------------------------------------
# 
# Classfier: Random forest
# Nr features: 15000
# 30 trees, max depth 100
#
# Nr items: 50 000
# -------------------------
# Score Exact: 58.2 +- 0.8
# Score in top 3: 71.8% +- 0.8
# Score in top 5: 75.7% +- 0.7
#
# Nr items: 100 000
# -------------------------
# Score Exact: 62.5% +- 0.3
# Score in top 3: 75.5% +- 0.2
# Score in top 5: 79.0% +- 0.2
#
# Nr items: 500 000
# -------------------------
# accuracy: 72.0 +- 0.1
# accuracy top 3: 82.5 +- 0.1
# accuracy top 5: 85.2 +- 0.1
#
## Nr items: 1M
# -------------------------
#exact: 76.2 % +- 0.1
#in top 3: 85.6 % +- 0.1
#in top 5: 87.8 % +- 0.1
#
#
# 20 trees, max depth 200:
# Nr items: 1M
# 
# exact: 78.0 % +- 0.04
# in top 3: 87.0 % +- 0.07
# in top 5: 89.0 % +- 0.1

# Nr y items:
# Occupation id / "yrkesbenamning": 
# SSYK Level 4: - (training data) / 429 (SCB)
# SSYK Level 3: - (training data) / 147 (SCB)
# SSYK Level 2: - (training data) / 46 (SCB)
# SSYK Level 1: - (training data) / 10 (SCB)

#classType = 'naive_bayes'
#classType = 'knn'
classType = 'random_forest'

usePreSampled = False
doCrossValidation = True
doFullModel = True
useStratifiedSampling = False # not implemented
useSSYKLevel = 4

nrItems = 1300000
nrFeatures = 15000
crossvalSplits = 5
rf_estimators = 20
rf_max_depth = 300

pathData = 'data/'
pathModels = 'models/'

filenames = [pathData + '2019.json', pathData + '2018.json']

if usePreSampled:
    filenames = [pathData + 'sample.json']

with open(pathData + 'occId_2_ssyk2012.json') as f:
    occId2ssyk = json.load(f)

with open(pathData + 'occId_2_name.json') as f:
    yrkeId2Name = json.load(f) 

def fit_transform_vec_tfid(x_all,nrFeatures=10000, df_min = 1, df_max=1.0):

    print('vectorizer')
    vectorizer = CountVectorizer(max_features=nrFeatures, min_df=df_min, max_df=df_max, stop_words=stopwords.words('swedish'))

    print('tfid')
    tfidfconverter = TfidfTransformer()

    print('fit_transform vectorizer')
    temp = vectorizer.fit_transform(x_all)
    print('toarray')
    x_all = temp#.toarray()
    
    print('fit_transform tfid')
    temp = tfidfconverter.fit_transform(x_all)
    print('toarray')
    x_all = temp#.toarray()

    return [x_all, vectorizer, tfidfconverter]

def accuracy_score_topX(y_test,resTop):
    nrCorrectTopX = 0
    for i,c in enumerate(y_test):
        if c in resTop[i]:
            nrCorrectTopX+=1

    return 1.0*nrCorrectTopX/len(y_test)


def iteration_train(X_train, y_train, nrFeatures, rf_estimators = 100, rf_max_depth = 30, nr_par = 12):

    print('train',len(X_train),'samples')

    [X_train, vectorizer,tfidfconverter] = fit_transform_vec_tfid(X_train, nrFeatures)

    print('classifier:',classType)

    if classType == 'random_forest':
        classifier = RandomForestClassifier(n_estimators=rf_estimators, random_state=0, max_depth = rf_max_depth, n_jobs=nr_par)
    elif classType == 'naive_bayes':
        classifier = MultinomialNB()
    elif classType == 'knn':
        classifier = KNeighborsClassifier(n_neighbors=3, n_jobs=nr_par)

    classifier.fit(X_train, y_train)

    print('completed')
    
    return [vectorizer, tfidfconverter, classifier]

def iteration_test(X_test, y_test, vectorizer, tfidconverter, classfier):

    print('test',len(X_test),'samples')

    X_test = vectorizer.transform(X_test)#.toarray()
    X_test = tfidfconverter.transform(X_test)#.toarray()

    y_pred = classifier.predict(X_test)
    y_predTop = classifier.predict_proba(X_test)

    rTop5 = np.argsort(y_predTop)[:,:-5-1:-1]
    resTop3 = []
    resTop5 = []

    for r in rTop5:
        rs = [classfier.classes_[i] for i in r]
        resTop3.append(rs[0:3])
        resTop5.append(rs)

    #print('confusion mat:\n',confusion_matrix(y_test,y_pred))
    #print('report:\n', classification_report(y_test,y_pred))
    score = accuracy_score(y_test, y_pred)
    scoreTop3 = accuracy_score_topX(y_test,resTop3)
    scoreTop5 = accuracy_score_topX(y_test,resTop5)

    print('accuracy:', score)
    print('accuracy top 3:', scoreTop3)
    print('accuracy top 5:', scoreTop5)

    return [score, scoreTop3, scoreTop5]

# 1. DATA HANDLING

print('loading',filenames)

ads = []

for filename in filenames:
    with open(filename) as fp:
        ads.extend(json.load(fp))

print('nr ads:',len(ads))

if usePreSampled == False: # assumes always sampling

    if useStratifiedSampling: # stratified based on occupations
        print('ERROR: not implemented')
    else: # uniform
        sampleIndices = random.sample(range(0,len(ads)), nrItems)
        ads = [ads[i] for i in sampleIndices]

    print('saving sampled data')

    with open(pathData + 'sample.json','w') as f:
        json.dump(ads,f)

print('nr ads:',len(ads))

X_text = []
X_headline = []
y_occId = []
y_ssyk = []
check_not_existing = []

#  removing items which have an older (not used anymore) occupation id
for ad in ads:
    
    #X_headline.append(ad['headline'])
    occId = int(ad['occupation']['legacy_ams_taxonomy_id'])

    if str(occId) in occId2ssyk:
        ssyk = int(occId2ssyk[str(occId)])
        X_text.append(ad['description']['text'])

        if useSSYKLevel == 1:
            ssyk = int(ssyk/1000)
        elif useSSYKLevel == 2:
            ssyk = int(ssyk/100)
        elif useSSYKLevel == 3:
            ssyk = int(ssyk/10)

        y_ssyk.append(ssyk)
    else:
        check_not_existing.append(str(occId))
        ssyk = -1

    #y_occId.append(occId)
    

del ads

X = np.array(X_text)
y = np.array(y_ssyk)

print('len(X)=',len(X))

del X_text, y_ssyk


# 2. CROSS VALIDATION

if doCrossValidation:
    skf = StratifiedKFold(n_splits=crossvalSplits)

    index = 0

    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    allres = []
    allresTop3 = []
    allresTop5 = []

    for train_index, test_index in skf.split(X, y):

        print('index:',index)
        #print("TRAIN:", train_index, "TEST:", test_index)
        X_train = X[train_index]
        y_train = y[train_index]

        [vectorizer, tfidfconverter, classifier] = iteration_train(X_train, y_train, nrFeatures, rf_estimators, rf_max_depth)
        del X_train, y_train

        X_test = X[test_index]
        y_test = y[test_index]

        [score, score_top3, score_top5] = iteration_test(X_test, y_test, vectorizer, tfidfconverter, classifier)
        del X_test, y_test, vectorizer, tfidfconverter, classifier
        
        allres.append(score)
        allresTop3.append(score_top3)
        allresTop5.append(score_top5)

        index+=1

    print('results cross-validation')
    
    print('exact:',100.0*np.mean(allres),'% +-',100.0*np.std(allres))
    print('in top 3:',100.0*np.mean(allresTop3),'% +-',100.0*np.std(allresTop3))
    print('in top 5:',100.0*np.mean(allresTop5),'% +-',100.0*np.std(allresTop5))


# 3. FULL MODEL TRAIN

if doFullModel:
    print('training on full set')

    #[score, score_top3, score_top5, vectorizer, tfidfconverter, classifier] = iteration_classify(X,y,X,y, nrFeatures, 100, None)
    [vectorizer, tfidfconverter, classifier] = iteration_train(X, y, nrFeatures, rf_estimators, rf_max_depth)

    pickle.dump(vectorizer, open(pathModels + "vector_"+str(nrItems)+"_"+str(nrFeatures)+".pickle", "wb"))
    pickle.dump(tfidfconverter, open(pathModels + "tfid_"+str(nrItems)+"_"+str(nrFeatures)+".pickle", "wb"))
    joblib.dump(value=classifier, filename=pathModels + 'model_' + classType + '_' + str(nrItems) + "_" + str(nrFeatures) + "_" + str(rf_max_depth) + ".gzip", compress=True) 

print('end')