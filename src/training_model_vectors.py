import numpy as np
import tensorflow as tf
import tensorflow.keras
from tensorflow.keras.layers.experimental.preprocessing import Normalization
import data_handling
import re
import string

from tensorflow.keras import layers
from tensorflow.keras import losses
from tensorflow.keras import preprocessing
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization


def custom_standardization(input_data):
  lowercase = tf.strings.lower(input_data)
  stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
  return tf.strings.regex_replace(stripped_html,
                                  '[%s]' % re.escape(string.punctuation),
                                  '')

def vectorize_text(text, label):
  text = tf.expand_dims(text, -1)
  return vectorize_layer(text), label

def vectorize_text_nolabel(text):
  text = tf.expand_dims(text, -1)
  return vectorize_layer(text)

#tf.config.threading.set_intra_op_parallelism_threads(14)

# load data
[X_text, y_ssyk] = data_handling.load_data()

# remap
unique_ssyk = set(y_ssyk)
map_ssyk = {}
remap_ssyk = {}
for i,u in enumerate(unique_ssyk):
  map_ssyk[u] = i
  remap_ssyk[i] = u

new_y = []
for y in y_ssyk:
  new_y.append(map_ssyk[y])

y_ssyk = new_y 
y_ssyk = np.asarray(y_ssyk).astype('int32').reshape((-1,1))
max_ssyk = max(y_ssyk)[0]

print(len(X_text))

train_end = int(3.0*len(X_text)/5.0)
val_end = int(4.0*len(X_text)/5.0)
test_end = len(X_text)

print('nr train:', train_end)
print('nr val:', val_end - train_end)
print('nr test:',test_end - val_end)

# train/test split
[X_train, y_train]  =   [X_text[0:train_end], y_ssyk[0:train_end]]
[X_val,  y_val]    =   [X_text[train_end:val_end], y_ssyk[train_end:val_end]]
[X_test,  y_test]    =   [X_text[val_end:test_end], y_ssyk[val_end:test_end]]

total_train_samples = 1.0*len(y_ssyk)
classes = np.unique(y_ssyk)
class_weights = {}

# in case we want to correct for uneven data distribution
for i,c in enumerate(classes):
  class_weights[i] = (1.0 * total_train_samples / sum(y_ssyk == c)[0]) / len(classes)

print('nr class weights:', len(class_weights))
print(class_weights)

texts_train = tf.constant(X_train)
labels_train = tf.constant(y_train)
texts_val = tf.constant(X_val)
labels_val = tf.constant(y_val)
texts_test = tf.constant(X_test)
labels_test = tf.constant(y_test)

raw_train_ds = tf.data.Dataset.from_tensor_slices((texts_train, labels_train))
raw_val_ds = tf.data.Dataset.from_tensor_slices((texts_val, labels_val))
raw_test_ds = tf.data.Dataset.from_tensor_slices((texts_test, labels_test))

for text_batch, label_batch in raw_train_ds.take(1):
  print("Text:", text_batch.numpy())
  print("Label:", label_batch.numpy())

max_features = 15000
ngrams = None#3
sequence_length = None#250

vectorize_layer = TextVectorization(
    #standardize=custom_standardization,
    max_tokens=max_features,
    ngrams=ngrams,
    output_mode='int',#'tf-idf'
#output_sequence_length=sequence_length
)

train_text = raw_train_ds.map(lambda x, y: x)
vectorize_layer.adapt(train_text)

element = next(iter(raw_train_ds))
first_text, first_label = element[0], element[1]
print("Text", first_text)
print("Label", first_label)
print("Vectorized review", vectorize_text(first_text, first_label))

#print("3979 ---> ",vectorize_layer.get_vocabulary()[3979])
#print('Vocabulary size: {}'.format(len(vectorize_layer.get_vocabulary())))

train_ds = raw_train_ds.map(vectorize_text)
val_ds = raw_val_ds.map(vectorize_text)
test_ds = raw_test_ds.map(vectorize_text)

AUTOTUNE = tf.data.experimental.AUTOTUNE

train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

# setup model

embedding_dim = 512#128
#max_features = 15000
sequence_length = 250

model = tf.keras.Sequential([
  layers.Embedding(max_features + 1, embedding_dim),
  layers.Dropout(0.2),
  layers.GlobalAveragePooling1D(),
  layers.Dropout(0.2),
  layers.Dense(max_ssyk + 1, activation='softmax')])

model.summary()

#model.compile(loss=losses.BinaryCrossentropy(from_logits=True), optimizer='adam', metrics=tf.metrics.BinaryAccuracy(threshold=0.0))
model.compile(loss=losses.SparseCategoricalCrossentropy(), optimizer='adam', metrics=['accuracy'])

epochs = 10
batch_size = 64

# train
history = model.fit(
    train_ds,
    batch_size=batch_size,
    validation_data=val_ds,
    epochs=epochs)
#    class_weight=class_weights)
#    callbacks=[tensorboard_callback])


# evaluate
loss_train, accuracy_train = model.evaluate(train_ds)
loss_val, accuracy_val = model.evaluate(val_ds)
loss_test, accuracy_test = model.evaluate(test_ds)

predictions = model.predict_classes(train_ds)
labels = [y.numpy()[0] for x,y in train_ds.take(-1)]
cm = tf.math.confusion_matrix(labels,predictions)

#model.predict_classes(train_ds)

print("Loss train / val / test: ", loss_train,'/', loss_val, '/', loss_test)
print("Accuracy train & val / val / test: ", accuracy_train, '/', accuracy_val, '/', accuracy_test)

X = ['OM UPPDRAGETNu söker Centric en barnsjuksköterska som kan jobba på barnavdelning för blod- och tumörsjukdomar i Uppsala i sommar. DIN PROFILVi välkomnar dig som är specialistutbildad barnsjuksköterska med minst två års yrkeserfarenhet. OM OSS​Centric Care har bemannat hälso-och sjukvården sedan år 2000 och är ett av Skandinaviens ledande bemanningsföretag med kontor i Göteborg, Stockholm, Malmö och Oslo. Vi erbjuder tillfällig och fast bemanning till privat och offentlig hälso- och sjukvård samt inom socialt arbete. Som konsult hos oss erbjuds du några av marknadens bästa lönevillkor, varierande uppdrag, en egen dedikerad kontaktperson samt ett unikt friskvårdsbidrag på upp till 4000 kr per kalenderår. Centric Care är ett auktoriserat bemanningsföretag som är medlem i Almega, Kompetensföretagen. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet, Kommunal, Unionen och Akademikerförbundet. Tillsammans med tjänstepensionsavtal och försäkringsskydd innebär detta en trygghet för dig som medarbetare.  Nyfiken på att veta mer? Kontakta oss redan idag. Välkommen!',
    'OM UPPDRAGET Nu söker Centric en barnsjuksköterska som kan jobba på barnavdelning för blod- och tumörsjukdomar i Uppsala i sommar. DIN PROFIL Vi välkomnar dig som är specialistutbildad barnsjuksköterska med minst två års yrkeserfarenhet. OM OSS ​Centric Care har bemannat hälso-och sjukvården sedan år 2000 och är ett av Skandinaviens ledande bemanningsföretag med kontor i Göteborg, Stockholm, Malmö och Oslo. Vi erbjuder tillfällig och fast bemanning till privat och offentlig hälso- och sjukvård samt inom socialt arbete. Som konsult hos oss erbjuds du några av marknadens bästa lönevillkor, varierande uppdrag, en egen dedikerad kontaktperson samt ett unikt friskvårdsbidrag på upp till 4000 kr per kalenderår. Centric Care är ett auktoriserat bemanningsföretag som är medlem i Almega, Kompetensföretagen. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet, Kommunal, Unionen och Akademikerförbundet. Tillsammans med tjänstepensionsavtal och försäkringsskydd innebär detta en trygghet för dig som medarbetare. Nyfiken på att veta mer? Kontakta oss redan idag. Välkommen!',
    'OM UPPDRAGET Nu söker en intensivvårdssjuksköterska som vill jobba på IVA i Hudiksvall i sommar Behov finns under hela sommarperioden Vi hjälper till med boende och resor vid behov! DINA ARBETSUPPGIFTER Sedvanliga arbetsuppgifter för intesivvårdssjuksköterska på IVA. DIN PROFIL Intensivvårdssjuksköterska med minst två år dokumenterad yrkseslivserfarenhet. OM OSS ​Centric Care har bemannat hälso-och sjukvården sedan år 2000 och är ett av Skandinaviens ledande bemanningsföretag med kontor i Göteborg, Stockholm, Malmö och Oslo. Vi erbjuder tillfällig och fast bemanning till privat och offentlig hälso- och sjukvård samt inom socialt arbete. Som konsult hos oss erbjuds du några av marknadens bästa lönevillkor, varierande uppdrag, en egen dedikerad kontaktperson samt ett unikt friskvårdsbidrag på upp till 4000 kr per kalenderår. Centric Care är ett auktoriserat bemanningsföretag som är medlem i Almega, Bemanningsföretagen. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet, Kommunal, Unionen och Akademikerförbundet. Tillsammans med tjänstepensionsavtal och försäkringsskydd innebär detta en trygghet för dig som medarbetare. Nyfiken på att veta mer om oss? Kontakta oss redan idag. Välkommen!',
]
#    'barnsjuksköterska',
#    'gymanasielärare',
#    'högstadielärare']

X_c = tf.constant(X)
#labels = 
raw_X_ds = tf.data.Dataset.from_tensor_slices(X_c)#((X_c, labels))
X_ds = raw_X_ds.map(vectorize_text_nolabel)

predictions = model.predict(X_ds)

k = 5
top_k_values, top_k_indices = tf.nn.top_k(predictions, k=k)

print(top_k_values)
print(top_k_indices)
print([remap_ssyk[ind] for ind in top_k_indices])

print('end')