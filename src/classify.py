import json
import numpy as np
import pickle
#import joblib
from sklearn.externals import joblib

import os
import sys
import help_functions

class classify(object):

    def load_model_baseline(self,filename_vec, filename_tfid, filename_model):
        
        self._vectorizer = pickle.load(open(filename_vec, "rb"))
        self._tfidconverter = pickle.load(open(filename_tfid,"rb"))
        self._classifier = joblib.load(filename_model)

    def classify_model_baseline(self,inputTexts):

        X = [help_functions.preprocessText(s) for s in inputTexts]
        X = self._vectorizer.transform(X)
        X = self._tfidconverter.transform(X)

        #res = classifier.predict(X)
        rTop = self._classifier.predict_proba(X)

        n = 5
        rTop = np.argsort(rTop)[:,:-n-1:-1]
        resTop5 = []
        for r in rTop:
            rs = [self._classifier.classes_[i] for i in r]
            resTop5.append(rs)

        return resTop5