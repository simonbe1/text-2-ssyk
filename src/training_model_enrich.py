# uses enriched data as features

import json
import data_handling
import numpy as np
#from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
#from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import scipy
from scipy.sparse import csr_matrix

# 16k train, 4k test: 61.6%
# 80k train, 20k test: 67.8%
# 300k train, 100k test: 73.6%
# 450k train, 150k test: 72.8% (rf classifier: estimators=20, max_depth=300)
# 450k train, 150k test: 74.1% (rf classifier: estimators=40, max_depth=300)
# 750k train, 250k test: 73.1% (rf classifier: estimators=20, max_depth=300)

# generate enriched data
generate_enrich = False

if generate_enrich:

    with open('config.json') as f:
        config = json.load(f)
    
    api_key = config['api-key']

    res = data_handling.load_and_make_enriched_train_set(api_key)

    print(len(res))

max_items = 1000000
[all_concepts, data] = data_handling.load_enriched_data(max_items)

print('total concepts:', len(all_concepts))
print('ads:', len(data))

# generate mappings to vector
concept2index = {}

for i,concept in enumerate(all_concepts):
    concept2index[concept[0]] = i

# generate input vectors
X_all = []#csr_matrix((len(data),len(all_concepts)))#[]
y_all = []

for i,ad in enumerate(data):
    concepts = ad['concepts']
    x = np.zeros(len(all_concepts))
    for c in concepts:
        #X_all[i,concept2index[c[0]]] = c[1]
        x[concept2index[c[0]]] = c[1]
    X_all.append(csr_matrix(x))
    y_all.append(ad['ssyk'])

del data

# convert to proper sparse matrix
X_all = scipy.sparse.vstack(X_all)

#print(len(X_all))
x_length = X_all.shape[0]

split_index = int(x_length * 0.75)
X_train = X_all[0:split_index]
y_train = y_all[0:split_index]
X_test = X_all[split_index:x_length]
y_test = y_all[split_index:len(y_all)]

length_x_train = X_train.shape[0]
length_x_test = X_test.shape[0]
# train classifier
print('train', length_x_train,'items')

rf_estimators = 20
rf_max_depth = 300
nr_par = 8

classifier = RandomForestClassifier(n_estimators=rf_estimators, random_state=0, max_depth = rf_max_depth, n_jobs=nr_par)
classifier.fit(X_train, y_train)

# test
print('test', length_x_test, 'items')

y_pred = classifier.predict(X_test)
score = accuracy_score(y_test, y_pred)

print(score)

