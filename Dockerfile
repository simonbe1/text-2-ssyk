FROM python:3.6

RUN mkdir /app

WORKDIR /app

COPY src/joblinks_main.py /app
COPY src/joblinks_classify.py /app
COPY src/help_functions.py /app
# if lfs copy from repo, else take from manually defined branch
#COPY models/model_random_forest_300000_15000.gzip /app
#COPY models/vector_300000_15000.pickle /app
#COPY models/tfid_300000_15000.pickle /app
ADD https://gitlab.com/joblinks/text-2-ssyk/-/raw/develop/models/model_random_forest_300000_15000.gzip /app
ADD https://gitlab.com/joblinks/text-2-ssyk/-/raw/develop/models/vector_300000_15000.pickle /app
ADD https://gitlab.com/joblinks/text-2-ssyk/-/raw/develop/models/tfid_300000_15000.pickle /app
COPY requirements.txt /app

RUN pip install -r requirements.txt

CMD ["python","joblinks_main.py"]
